import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { PiClockCounterClockwise } from 'react-icons/pi';
import { IoArrowRedo } from "react-icons/io5";
import styles from './balanceDisplay.module.css';

const BalanceDisplay = () => {
    return (
      <Container className={`${styles.balanceContainer} balance-display`}>
        <Row>
          <Col>
            <h3 className={styles.balanceTitle}>Balance</h3>
            <p className={styles.balanceAmount}>$2,748.00</p>
          </Col>
          <Col className={styles.iconContainer}>
            <div className={styles.iconStyle}>
              <PiClockCounterClockwise />
            </div>
            <div className={styles.iconStyle}>
              <IoArrowRedo />
            </div>
          </Col>
        </Row>
      </Container>
    );
};

export default BalanceDisplay;