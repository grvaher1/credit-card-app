import React, { useState } from 'react';

import { Row, Col } from 'react-bootstrap';

import styles from './transactionItem.module.css';

const TransactionItem = ({ icon, service, amount, subtitle, iconColor, additionalInfo }) => {
  const [showAdditionalInfo, setShowAdditionalInfo] = useState(false);

  const dynamicIconStyle = {
    backgroundColor: iconColor,
    border: `2px solid ${iconColor}`
  };

  const handleRowClick = () => {
    setShowAdditionalInfo(!showAdditionalInfo)
  }

  return (
    <Row className={`my-4 align-items-center ${styles.rowStyle}`} onClick={handleRowClick}>
      <Col xs={2} className="text-center">
        <div className={`${styles.iconStyle}`} style={dynamicIconStyle}>
          {icon}
        </div>
      </Col>
      <Col xs={6}>
        <div>{service}</div>
        <div className={styles.subtitleStyle}>{subtitle}</div>
        {(showAdditionalInfo && additionalInfo) && <div className={styles.additionalInfoStyle}>{additionalInfo}</div>}
      </Col>
      <Col xs={4} className={`text-end ${styles.amountStyle}`}>
        {amount}
      </Col>
    </Row>
  );
};

export default TransactionItem;