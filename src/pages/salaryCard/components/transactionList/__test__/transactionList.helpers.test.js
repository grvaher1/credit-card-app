import { getAdditionalInfoDate, getPreviousDate } from '../transactionList.helpers';
import moment from 'moment';

describe('getAdditionalInfoDate', () => {
  it('formats a valid date correctly', () => {
    const date = '21/09/2023';
    const formattedDate = getAdditionalInfoDate(date);
    expect(formattedDate).toBe('21/09/2023');
  });

  it('returns invalid date for an incorrect date string', () => {
    const invalidDate = 'invalid-date';
    const formattedDate = getAdditionalInfoDate(invalidDate);
    expect(formattedDate).toBe('Invalid date');
  });
});

describe('getPreviousDate', () => {
  it('subtracts one day from the given date', () => {
    const date = moment('2023-09-21');
    const previousDate = getPreviousDate()(date);
    expect(previousDate.format('YYYY-MM-DD')).toBe('2023-09-20');
  });
});
