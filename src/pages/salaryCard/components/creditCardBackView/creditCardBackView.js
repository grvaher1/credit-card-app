import React from 'react';
import { Card } from 'react-bootstrap';
import styles from './creditCardBackView.module.css';

const CreditCardBackView = (props) => {
    const { onCardClick } = props;
  
    return (
      <Card className={`mb-3 ${styles.cardStyle}`} onClick={onCardClick}>
        <div className={`${styles.blackStripStyle}`}></div>
        <div className={`${styles.numberBoxStyle}`}>
          123
        </div>
      </Card>
    );
};

export default CreditCardBackView;