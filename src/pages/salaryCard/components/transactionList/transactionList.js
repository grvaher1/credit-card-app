
import React, { useState, useCallback, useEffect } from 'react';

import moment from 'moment';
import { Container } from 'react-bootstrap';

import { TODAYS_DATA } from './transactionList.constants';
import TransactionItem from '../transactionItem';
import { getPreviousDate, getPrevDayData } from './transactionList.helpers';

import styles from './transactionList.module.css';

const TransactionList = (props) => {
  const [transactions, setTransactions] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [currentDate, setCurrentDate] = useState(moment());
  const [isExpanded, setIsExpanded] = useState(false);

  useEffect(() => {
    //make an api call to get the initial data
    setTransactions([TODAYS_DATA])
  }, [])

  const [containerHeight, setContainerHeight] = useState('300px');

  const handleExpandScreen = () => {
    const { handleExpandTrasactionView, transformTitle } = props;
    setContainerHeight(containerHeight === '300px' ? '90vh' : '300px');
    setIsExpanded(!isExpanded)
    handleExpandTrasactionView(!transformTitle)
  };


  const fetchMoreTransactions = useCallback(async () => {
    if (isLoading) return;

    setIsLoading(true);
    //emulate api call to fetch data for the next day
    setTimeout(() => {
      const previousDayTransactions = getPrevDayData(currentDate);
      setCurrentDate(getPreviousDate());
      setTransactions([...transactions, previousDayTransactions]);
      setIsLoading(false);
    }, 1000);
  }, [isLoading]);


  const handleScroll = (event) => {
    const { scrollTop, clientHeight, scrollHeight } = event.currentTarget;

    const bottom = scrollHeight - scrollTop <= clientHeight + 5;

    if (bottom) {
      fetchMoreTransactions();
    }
  };

  const containerStyle = {
    position: isExpanded ? 'fixed' : '',
    bottom: 0,
    left: 0,
    right: 0,
    height: containerHeight,
    overflowY: 'auto',
    zIndex: 1000,
    backgroundColor: '#1C2641',
    transition: 'height 2s ease-in-out',
    border: '1px solid #2B3553',
    borderRadius: '5%',
  };

  return (
    <Container
      style={containerStyle}
      className='my-3'
      onScroll={handleScroll}
    >
      <div className={styles.topLineContainer} onClick={handleExpandScreen}>
        <div className={styles.topLine} onClick={handleExpandScreen}></div>
      </div>
      {
        transactions.map(element => {
          return (
            <React.Fragment key={element?.date}>
              <h5>{element?.date}</h5>
              {
                element?.data.map(item => {
                  return (
                    <TransactionItem
                      key={item.id}
                      service={item.service}
                      amount={item.amount}
                      subtitle={item.subtitle}
                      icon={item.icon}
                      iconColor={item.iconColor}
                      additionalInfo={item.additionalInfo}
                    />
                  )
                })
              }
            </React.Fragment>
          )
        })
      }
    </Container>
  );
};

export default TransactionList;