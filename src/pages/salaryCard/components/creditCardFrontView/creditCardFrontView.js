import React from 'react';
import { FaCcMastercard } from "react-icons/fa";
import { IoIosWifi } from "react-icons/io";
import styles from './creditCardFrontView.module.css';

const CreditCardFrontView = (props) => {
  const { onCardClick } = props;

  return (
    <div className={`card mb-3 ${styles.gradientStyle}`} onClick={onCardClick}>
      <div className={`${styles.cardDetail}`}>
        <div className={`${styles.bankName}`}>CB</div>
        <div className={`${styles.divider}`}>|</div>
         <div className={`${styles.cardDate}`}>
         Universal Bank</div>
      </div>
      <div style={{ padding: '30px' }}>
        <div className={`${styles.iconBox}`}></div>
        <IoIosWifi className={`${styles.wifiIcon}`} />
      </div>
      <div className="card-body">
        <h5 className={`${styles.cardTitle}`}>5489 7452 5726 9827</h5>
        <p className={`${styles.cardDate}`}>04/24</p>
        <FaCcMastercard className={`${styles.mastercardIcon}`} />
      </div>
    </div>
  );
}

export default CreditCardFrontView;