import moment from 'moment';

import { FaYoutube } from 'react-icons/fa';
import { IoMusicalNote } from "react-icons/io5";

export const getAdditionalInfoDate = date => moment(date, 'DD/MM/YYYY').format('DD/MM/YYYY');

export const getPreviousDate = () => prevDate => prevDate.clone().subtract(1, 'days');

export const getPrevDayData = (date) => {
    const data = {
        date: moment(date).format('Do MMMM'),
        data: getSampleTrasactionData(date)
    }
    return data;
}

export const getSampleTrasactionData = (date) => {
    return [
        {
            service: "Amazon",
            amount: "+ $49.00",
            subtitle: "Service",
            icon: <IoMusicalNote />,
            iconColor: '#F3AA38',
            additionalInfo: `xxxxxxxxxx4586 ${getAdditionalInfoDate(date)} 10:12 A.M.`,
            id: '1'
        },
        {
            service: "Netflix",
            amount: "+ $180.00",
            subtitle: "Service",
            icon: <FaYoutube />,
            iconColor: '#DE3A84',
            additionalInfo: `xxxxxxxxxx4586 ${getAdditionalInfoDate(date)} 10:12 A.M.`,
            id: '2'
        },
        {
            service: "Amazon Prime",
            amount: "+ $190.00",
            subtitle: "Service",
            icon: <FaYoutube />,
            iconColor: '#DE3A84',
            additionalInfo: `xxxxxxxxxx4586 ${getAdditionalInfoDate(date)} 10:12 A.M.`,
            id: '3'
        },
    ]
}
