import moment from 'moment';
import { getAdditionalInfoDate } from './transactionList.helpers';

import { FaCar, FaYoutube } from 'react-icons/fa';
import { IoArrowRedo, IoMusicalNote } from "react-icons/io5";

export const TODAYS_DATA = {
    date: 'Today',
    data: [
      {
        service: "Card to card",
        amount: "+ $143.00",
        subtitle: "Maria",
        icon: <IoArrowRedo />,
        iconColor: '#DE3A84',
        additionalInfo: `xxxxxxxxxx4586 ${getAdditionalInfoDate(moment())} 10:12 A.M.`,
        id: '4'
      },
      {
        service: "Uber",
        amount: "+ $249.00",
        subtitle: "Service",
        icon: <IoMusicalNote />,
        iconColor: '#F3AA38',
        additionalInfo: `xxxxxxxxxx4586 ${getAdditionalInfoDate(moment())} 10:12 A.M.`,
        id: '5',
      },
      {
        service: "Apple Music",
        amount: "+ $90.00",
        subtitle: "Online",
        icon: <FaCar />,
        iconColor: '#2F85F7',
        additionalInfo: `xxxxxxxxxx4586 ${getAdditionalInfoDate(moment())} 10:12 A.M.`,
        id: '6',
      },
      {
        service: "Youtube Premium",
        amount: "+ $100.00",
        subtitle: "Service",
        icon: <FaYoutube />,
        iconColor: '#DE3A84',
        additionalInfo: `xxxxxxxxxx4586 ${getAdditionalInfoDate(moment())} 10:12 A.M.`,
        id: '7',
      },
      {
        service: "Apple Music",
        amount: "+ $90.00",
        subtitle: "Online",
        icon: <FaCar />,
        iconColor: '#2F85F7',
        additionalInfo: `xxxxxxxxxx4586 ${getAdditionalInfoDate(moment())} 10:12 A.M.`,
        id: '8',
      },
      {
        service: "Youtube Premium",
        amount: "+ $100.00",
        subtitle: "Service",
        icon: <FaYoutube />,
        iconColor: '#DE3A84',
        additionalInfo: `xxxxxxxxxx4586 ${getAdditionalInfoDate(moment())} 10:12 A.M.`,
        id: '9',
      },
      {
        service: "Apple Music",
        amount: "+ $90.00",
        subtitle: "Online",
        icon: <FaCar />,
        iconColor: '#2F85F7',
        additionalInfo: `xxxxxxxxxx4586 ${getAdditionalInfoDate(moment())} 10:12 A.M.`,
        id: '10',
      },
      {
        service: "Youtube Premium",
        amount: "+ $100.00",
        subtitle: "Service",
        icon: <FaYoutube />,
        iconColor: '#DE3A84',
        additionalInfo: `xxxxxxxxxx4586 ${getAdditionalInfoDate(moment())} 10:12 A.M.`,
        id: '11',
      }
    ]
  }