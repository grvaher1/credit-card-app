import React, { useState, useCallback } from 'react';

import { Container, Row, Col } from 'react-bootstrap';

import CreditCardFrontView from './components/creditCardFrontView';
import CreditCardBackView from './components/creditCardBackView';
import BalanceDisplay from './components/balanceDisplay';
import TransactionList from './components/transactionList';

import styles from './salaryCard.module.css';


const SalaryCard = () => {
    const [showFront, setShowFront] = useState(true)
    const [transformTitle, setTransformTitle] = useState(false);
    const [animationClass, setAnimationClass] = useState('');

    const handleCardClick = useCallback(() => {
        setAnimationClass(styles.fadeOut);
        setTimeout(() => {
            setShowFront(!showFront);
            setAnimationClass(styles.fadeIn);
        }, 500);
    }, [setAnimationClass, showFront]);

    const cardWrapperStyle = {
        display: 'flex',
        justifyContent: 'center',
        width: '100%',
        position: 'relative',
    };

    const handleExpandTrasactionView = useCallback(() => {
        setTransformTitle(!transformTitle);
    }, [transformTitle]);

    return (
        <Container className={`${styles.mainContent} p-2`}>
            <Row>
                <Col>
                    <header className={transformTitle ? "d-flex justify-content-center align-items-center" : "pl-4 w-25"}>
                        <h1 className={transformTitle ? styles.titleSmall : styles.titleBig}>Salary card</h1>
                    </header>
                    <div style={cardWrapperStyle} className={animationClass}>
                        {showFront ? (
                            <CreditCardFrontView onCardClick={handleCardClick} />
                        ) : (
                            <CreditCardBackView onCardClick={handleCardClick} />
                        )}
                    </div>
                    <BalanceDisplay />
                    <TransactionList
                        transformTitle={transformTitle}
                        handleExpandTrasactionView={handleExpandTrasactionView} />
                </Col>
            </Row>
        </Container>
    )
}

export default SalaryCard;